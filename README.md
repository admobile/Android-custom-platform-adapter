## Android自定义平台适配器对接文档

[TOC]



## 1. 概述

尊敬的开发者，欢迎您使用ADSuyi广告SDK自定义适配广告平台。

操作系统及要求：使用Android Studio；Android4.1及以上（minSdkVersion 16及以上）主要按所适配渠道最低版本要求;

自定义适配器平台支持广告类型：开屏，横幅，插屏，激励视频，信息流；

竞价仅支持开屏、插屏、激励视频、信息流；

## 2. 添加SDK到工程中

### 2.1 添加仓库地址

首先需要在项目的build.gradle文件中引入如下配置：

```java
allprojects {
    repositories {
        ...
        // ADSuyi渠道依赖
        maven { url "https://maven.admobile.top/repository/maven-releases/" }
    }
}
```

### 2.2 添加ADSuyiSdk和需要的AdapterSdk到主项目中

```java
    ...
    // ADSuyiSdk核心库是必须导入的
    implementation "cn.admobiletop.adsuyi.ad:core:3.9.6.07123"

    // 天目，必须的
    implementation "cn.admobiletop.adsuyi.adapter:tianmu:2.2.6.08191"

    // 导入自定义的广告渠道，如demo中自定义的优量汇渠道module
    // 广点通适配器
    implementation project(":gdtadapter")
    // 广点通广告sdk
    implementation(name: 'gdt-4.603.1473', ext: 'aar')
    ...
```

### 2.3 添加ADSuyi核心库和必要的库到你所适配的核心库中

```java
dependencies {
    ...
    // ⚠️使用编译时依赖 避免和自己项目本身的依赖冲突，同时不影响适配器的编译⚠️
    compileOnly "cn.admobiletop.adsuyi.ad:core:3.9.6.07123"
    compileOnly(name: 'gdt-4.603.1473', ext: 'aar')
}
```

### 2.4 添加渠道的混淆配置

根据渠道提供的混淆进行配置。

### 2.5 配置渠道FileProvider配置

根据三方广告渠道提供的Provider进行配置，否则会影响到下载类广告。

## 3. 在ADSuyi运营后台增加自定义适配平台

1、**广告平台**：因ADSuyi与运营后台的通信是依赖于双端协定的平台名称来进行逻辑处理，新增自定义平台尚未开通开发者后台自主添加功能，如有需要请联系**ADSuyi运营同学**协助添加；需谨记在运营后台添加的广告平台名称字段，该字段在自定义适配器开发时需要用到且必须与后台所填一致，否则会有广告加载异常问题；在后台添加新平台时需要添加该三方平台的申请的`AppId`和`AppKey`，该参数用于SDK端三方平台SDK初始化；

2、**广告类型**：因自定义平台适配器不能增加新的广告类型，故只能使用现有广告类型来进行平台广告的添加，在添加自定义平台广告位时，需严格按照平台的广告类型创建广告位id，现有广告类型（模板，自渲染，模板2.0等），如不清楚平台所示广告为何种类型时，请咨询我们技术同学；**自定义平台适配器开发时，需明确该类为何种广告类型的加载器**；

3、**平台及广告加载器注册**：自定义适配器平台开发中，需要继承对应的初始化父类及广告类型适配器父类，并应在load方法中调用注册方法来告知主SDK注册的平台及广告类型；

4、**初始化类**：自定义平台初始化类继承自自定义适配平台SDK，应在父类开放方法实现中来初始化三方平台SDK；

5、**广告加载类**：自定义适配平台加载器类中，需要继承自适配平台SDK依赖库对应广告类型父类，并在子类中实现响应加载方法及展示方法（部分广告类型无展示方法，**仅需实现请求方法且务必正确调用相应方法确保广告正常展示**），方法中有广告请求参数`adapterParams`，根据平台加载广告所需参数从`adapterParams`拿到响应参数；

6、**广告回调**：因ADSuyi需要根据平台回调时机来统计广告相关数据，故开发者在适配器开发过程中，需要根据平台广告回调中，调用`call`开头的相关方法，告知主SDK端进行数据统计（如：-`callSuccess`广告获取成功时调用此方法），需严格按照对应回调调用相应方法；

## 4. 自定义适配器平台示例

以下自定义广告适配示例采用已适配平台：优量汇（gdt）；demo中已适配优量汇（gdt）平台；

## 5. 广告平台的初始化

新建项目时需要注意包名，必须按照如下规则进行设置，这里以广点通为例，其中gdt为所适配广告平台的别名。

```java
cn.admobiletop.adsuyi.adapter.gdt
```

**在gdt包目录下新建ADSuyiIniter（固定类名，请勿调整）类，并继承ADBaseIniter初始化基础类：**

`ADBaseIniter`接口

| 方法名        | 介绍 |
| ------------ | ---- |
| init(Context context, String appId, String appKey, ADSuyiInitConfig config) | 初始化方法。参数说明：context（上下文对象）、appId（广告平台初始化id）、appKey（广告平台初始化key）、config（初始化配置）。 |
| getAdapterVersion() | 当前适配器版本号，请使用三方广告版本号+适配日期+版本，如：4.603.1473.11281，4.603.1473：优量汇版本号，1128：是适配日期，1：版本号。 |
| setPersonalizedAdEnabled(boolean personalizedAdEnabled) | 个性化开关。参数说明：personalizedAdEnabled（true：开启、false：关闭，默认开启）|
| isClientBid() | 是否支持客户端竞价，支持请返回true，默认为false。 |
| isServerBid() | 是否支持服务端竞价，支持请返回true，默认为false。 |
| isParallelLoad() | 是否支持并发请求，支持请返回true，默认为true。 |
| callInitSuccess() | 初始化成功请回调该方法，一遍广告平台都支持初始化和请求广告共同发起，请查看demo直接回调。 |
| callInitFailed() | 初始化失败请回调该方法。 |


`ADSuyiInitConfig`接口

| 对象             | 方法        | 返回值  | 说明 |
| ---------------- | ---------------------- | ------- | ------------------ |
| ADSuyiInitConfig | isAgreePrivacyStrategy | Boolean | 是否同意隐私政策 |
|                  | debug                  | boolean  | 是否为测试模式 |
|                  | isCanUseLocation       | boolean | 能否使用定位数据 |
|                  | isCanUsePhoneState     | boolean | 能否使用设备信息 |
|                  | isCanUseWifiState      | boolean | 能否使用wifi信息 |
|                  | isCanUseOaid           | boolean | 能否使用OAID |


## 6. 广告平台广告的对接

### 6.1 开屏广告

**创建SplashAdLoader（固定类名，请勿调整）开屏广告加载器，请继承ADSplashLoader对象：**

| 方法名        | 介绍 |
| ------------ | ---- |
| adapterLoadAd(Context context, String positionId, ADExtraData adExtraData) | 加载广告。参数说明：context（上下文对象）、positionId（广告位id）、adExtraData（额外参数） |
| adapterShow(ViewGroup container) | 广告展示。参数说明：container（父布局容器） |
| adapterBiddingResult(int bidCode, ArrayList<Double> hbPriceList) | 竞价结果通知。参数说明：bidCode（竞价结果状态码）、hbPriceList（三方广告价格排序对象，单位：元） |
| adapterRelease() | 销毁操作方法。 |

**开屏广告回调方法**

| 方法名        | 介绍 |
| ------------ | ---- |
| callSuccess() | 通知广告获取。 |
| callSuccess(int bidPrice) | 通知竞价广告获取。参数说明：bidPrice（广告位价格，单位：分） |
| callExpose() | 通知广告曝光。 |
| callTick(int l) | 通知广告剩余倒计时时长。参数说明：l（倒计时剩余，单位：秒） |
| callClick() | 通知广告点击。 |
| callSkip() | 通知广告跳过。 |
| callClose() | 通知广告关闭。 |
| callFailed(int code, String message) | 通知错误。参数说明：code（错误码）、message（错误说明） |


### 6.2 横幅广告

**创建 BannerAdLoader（固定类名，请勿调整） 横幅广告加载器，请继承ADBannerLoader对象：**

| 方法名        | 介绍 |
| ------------ | ---- |
| adapterLoadAd(Context context, String positionId, ADExtraData adExtraData) | 加载广告。参数说明：context（上下文对象）、positionId（广告位id）、adExtraData（额外参数） |
| adapterShow(ViewGroup container) | 广告展示。参数说明：container（父布局容器） |
| adapterBiddingResult(int bidCode, ArrayList<Double> hbPriceList) | 竞价结果通知。参数说明：bidCode（竞价结果状态码）、hbPriceList（三方广告价格排序对象，单位：元） |
| adapterRelease() | 销毁操作方法。 |

**横幅广告回调方法**

| 方法名        | 介绍 |
| ------------ | ---- |
| callSuccess() | 通知广告获取。 |
| callSuccess(int bidPrice) | 通知竞价广告获取。参数说明：bidPrice（广告位价格，单位：分） |
| callExpose() | 通知广告曝光。 |
| callClick() | 通知广告点击。 |
| callClose() | 通知广告关闭。 |
| callFailed(int code, String message) | 通知错误。参数说明：code（错误码）、message（错误说明） |


### 6.3 插屏广告

**创建 InterstitialAdLoader（固定类名，请勿调整） 插屏广告加载器，请继承ADInterstitialLoader对象：**

| 方法名        | 介绍 |
| ------------ | ---- |
| adapterLoadAd(Context context, String positionId, ADExtraData adExtraData) | 加载广告。参数说明：context（上下文对象）、positionId（广告位id）、adExtraData（额外参数） |
| adapterShow(Context context) | 广告展示。参数说明：context（上下文对象） |
| adapterBiddingResult(int bidCode, ArrayList<Double> hbPriceList) | 竞价结果通知。参数说明：bidCode（竞价结果状态码）、hbPriceList（三方广告价格排序对象，单位：元） |
| adapterRelease() | 销毁操作方法。 |

**插屏广告回调方法**

| 方法名        | 介绍 |
| ------------ | ---- |
| callSuccess() | 通知广告获取。 |
| callSuccess(int bidPrice) | 通知竞价广告获取。参数说明：bidPrice（广告位价格，单位：分） |
| callExpose() | 通知广告曝光。 |
| callClick() | 通知广告点击。 |
| callClose() | 通知广告关闭。 |
| callFailed(int code, String message) | 通知错误。参数说明：code（错误码）、message（错误说明） |

### 6.4 激励视频广告

**创建 RewardVodAdLoader（固定类名，请勿调整） 激励视频广告加载器，请继承ADRewardLoader对象：**

| 方法名        | 介绍 |
| ------------ | ---- |
| adapterLoadAd(Context context, String positionId, ADExtraData adExtraData) | 加载广告。参数说明：context（上下文对象）、positionId（广告位id）、adExtraData（额外参数） |
| adapterShow(Context context) | 广告展示。参数说明：context（上下文对象） |
| adapterBiddingResult(int bidCode, ArrayList<Double> hbPriceList) | 竞价结果通知。参数说明：bidCode（竞价结果状态码）、hbPriceList（三方广告价格排序对象，单位：元） |
| adapterRelease() | 销毁操作方法。 |

**激励视频广告回调方法**

| 方法名        | 介绍 |
| ------------ | ---- |
| callSuccess() | 通知广告获取。 |
| callSuccess(int bidPrice) | 通知竞价广告获取。参数说明：bidPrice（广告位价格，单位：分） |
| callExpose() | 通知广告曝光。 |
| callClick() | 通知广告点击。 |
| callClose() | 通知广告关闭。 |
| callReward() | 通知广告奖励。 |
| callReward(Map<String, Object> map) | 通知广告关闭。参数说明：map（激励视频额外参数 |
| callFailed(int code, String message) | 通知错误。参数说明：code（错误码）、message（错误说明） |
| callVideoCache() | 通知视频缓存成功。 |
| callVideoComplete() | 通知视频播放完成。 |
| callVideoError(int code, String msg) | 通知视频播放异常。参数说明：code（错误码）、message（错误说明） |

### 6.5 信息流广告

**创建 NativeAdLoader（固定类名，请勿调整） 信息流广告加载器，请继承ADNativeLoader对象：**

| 方法名        | 介绍 |
| ------------ | ---- |
| adapterLoadExpressAd(Context context, String positionId, ADExtraData adExtraData) | 加载信息流模版广告。参数说明：context（上下文对象）、positionId（广告位id）、adExtraData（额外参数） |
| adapterLoadNativeAd(Context context, String positionId, ADExtraData adExtraData) | 加载信息流自渲染广告。参数说明：context（上下文对象）、positionId（广告位id）、adExtraData（额外参数） |
| adapterRelease() | 销毁操作方法。 |

**信息流广告回调方法**

| 方法名        | 介绍 |
| ------------ | ---- |
| callSuccess(List<ADNativeExpressInfo> infos) | 通知信息流模版广告获取成功。参数说明：context（信息流模版广告对象） |
| callSuccess(List<ADNativeInfo> infos) | 通知信息流自渲染广告获取成功。参数说明：context（信息流自渲染广告对象） |
| callFailed(int code, String message) | 通知错误。参数说明：code（错误码）、message（错误说明） |

#### 6.5.1 信息流模版广告

**创建 NativeExpressInfo 信息流模版广告对象，请继承ADNativeExpressInfo<T>对象，泛形T请传入当前渠道模版广告对象：**

| 方法名        | 介绍 |
| ------------ | ---- |
| getNativeExpressAdView(ViewGroup container) | 获取广告对象。参数说明：container（父容器） |
| render(ViewGroup container) | 渲染方法。参数说明：container（父容器） |
| adapterBiddingResult(int bidCode, ArrayList<Double> hbPriceList) | 竞价结果通知。参数说明：bidCode（竞价结果状态码）、hbPriceList（三方广告价格排序对象，单位：元） |

**信息流模版广告对象回调方法**

| 方法名        | 介绍 |
| ------------ | ---- |
| callExpose() | 通知广告曝光。 |
| callClick() | 通知广告点击。 |
| callClose() | 通知广告关闭。 |
| callVideoLoad() | 通知视频加载中。 |
| callVideoStart() | 通知视频开始播放。 |
| callVideoPause() | 通知视频暂停播放。 |
| callVideoFinish() | 通知视频播放完成。 |
| callVideoError(int code, String msg) | 通知视频播放异常。参数说明：code（错误码）、message（错误说明） |

#### 6.5.2 信息流自渲染广告

**创建 NativeInfo 信息自渲染广告对象，请继承ADNativeInfo<T>对象，泛形T请传入当前渠道自渲染广告对象：**

| 方法名        | 介绍 |
| ------------ | ---- |
| getMediaView(ViewGroup container) | 获取广告视频视图。参数说明：container（父容器） |
| registerViewForInteraction(ViewGroup viewGroup, View... actionViews) | 注册曝光&点击事件。参数说明：viewGroup（父容器）、actionViews（可点击布局） |
| registerCloseView(View closeView) | 注册关闭按钮事件。参数说明：closeView（关闭按钮布局） |
| adapterBiddingResult(int bidCode, ArrayList<Double> hbPriceList) | 竞价结果通知。参数说明：bidCode（竞价结果状态码）、hbPriceList（三方广告价格排序对象，单位：元） |
| setTitle(String s) | 设置标题。 |
| setDesc(String s) | 设置描述。 |
| setActionType(String s) | 设置行动类型。 |
| setCtaText(String s) | 设置行动按钮。 |
| setAppInfo(ADSuyiAppInfo appInfo) | 设置六要素。 |
| setIconUrl(String s) | 设置icon。 |
| setImageUrl(String s) | 设置单图。 |
| setImageUrlList(List<String> imageUrlList) | 设置多图。 |
| setIsVideo(boolean b) | 设置是否支持视频。 |
| setHasMediaView(boolean b) | 设置是否有视频布局。 |
| setPlatformIcon(int drawable) | 设置平台logo。 |

**ADSuyiAppInfo 信息流自渲染六要素**

| 方法名        | 介绍 |
| ------------ | ---- |
| setName(String s) | 设置应用名。 |
| setDeveloper(String s) | 设置开发者。 |
| setVersion(String s) | 设置版本号。 |
| setPermissionsUrl(String s) | 设置权限地址。 |
| setPrivacyUrl(String s) | 设置隐私条款地址。 |
| setDescriptionUrl(String s) | 设置描述地址。 |
| setIcp(String s) | 设置icp备案号。 |
| setSize(long l) | 设置应用大小。 |

**信息流自渲染广告对象回调方法**

| 方法名        | 介绍 |
| ------------ | ---- |
| callExpose() | 通知广告曝光。 |
| callClick() | 通知广告点击。 |
| callClose() | 通知广告关闭。 |
| callVideoLoad() | 通知视频加载中。 |
| callVideoStart() | 通知视频开始播放。 |
| callVideoPause() | 通知视频暂停播放。 |
| callVideoFinish() | 通知视频播放完成。 |
| callVideoError(int code, String msg) | 通知视频播放异常。参数说明：code（错误码）、message（错误说明） |


### 6.6 额外参数

**ADExtraData 额外参数**

| 方法名        | 介绍 |
| ------------ | ---- |
| isAdShakeDisable() | 是否支持摇一摇。仅开屏 |
| isImmersive() | 是否支持沉浸。仅开屏 |
| getAdWidth() | 获取广告宽度。仅开屏、信息流、横幅，0为自适应 |
| getAdHeight() | 获取广告高度。仅开屏、信息流、横幅，0为自适应 |
| getContentSize() | 获取广告内容大小。仅插屏，1：半屏，2全屏 |
| isMute() | 是否静音。true：静音，false：不静音。仅信息流、插屏、激励视频 |
| getRewardUserId() | 获取激励视频用户id。仅激励视频 |
| getRewardCustom() | 获取激励视频额外参数。仅激励视频 |
| getAdCount() | 获取请求广告数量。仅信息流 |
| getExtraMap() | 获取额外参数 |

### 6.7 获取设备标识

**ADSuyiSdk**

| 方法名        | 介绍 |
| ------------ | ---- |
| ADSuyiSdk.getInstance().getOAID() | 获取oaid |
| ADSuyiSdk.getInstance().getAndroidId(Context context) | 获取android |
| ADSuyiSdk.getInstance().getLocation(Context context) | 获取经纬度 |
| ADSuyiSdk.getInstance().getMac(Context context) | 获取mac |
| ADSuyiSdk.getInstance().getImei(Context context) | 获取imei |
