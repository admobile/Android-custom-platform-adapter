package cn.admobiletop.adsuyi.adapter.gdt;

import android.content.Context;

import com.qq.e.ads.rewardvideo.RewardVideoAD;
import com.qq.e.ads.rewardvideo.RewardVideoADListener;
import com.qq.e.comm.util.AdError;

import java.util.ArrayList;
import java.util.Map;

import cn.admobiletop.adsuyi.ad.adapter.bean.ADExtraData;
import cn.admobiletop.adsuyi.ad.adapter.loader.ADRewardLoader;
import cn.admobiletop.adsuyi.adapter.gdt.utils.BidPriceUtil;
import cn.admobiletop.adsuyi.bid.ADSuyiBidLossCode;

/**
 * @author maipian
 * @description 描述
 * @date 10/12/24
 */
public class RewardVodAdLoader extends ADRewardLoader {

    private RewardVideoAD rewardVideoAD;

    @Override
    public void adapterLoadAd(Context context, String positionId, ADExtraData adExtraData) {
        rewardVideoAD = new RewardVideoAD(
                context,
                positionId,
                new RewardVideoADListener() {
                    @Override
                    public void onADLoad() {
                        callSuccess(rewardVideoAD.getECPM());
                    }

                    @Override
                    public void onVideoCached() {
                        callVideoCache();
                    }

                    @Override
                    public void onADShow() {

                    }

                    @Override
                    public void onADExpose() {
                        callExpose();
                    }

                    @Override
                    public void onReward(Map<String, Object> map) {
                        callReward(map);
                    }

                    @Override
                    public void onADClick() {
                        callClick();
                    }

                    @Override
                    public void onVideoComplete() {
                        callVideoComplete();
                    }

                    @Override
                    public void onADClose() {
                        callClose();
                    }

                    @Override
                    public void onError(AdError adError) {
                        if (adError != null) {
                            callFailed(adError.getErrorCode(), adError.getErrorMsg());
                        }
                    }
                },
                true
        );
        rewardVideoAD.loadAD();
    }

    @Override
    public void adapterBiddingResult(int bidCode, ArrayList<Double> hbPriceList) {
        if (rewardVideoAD == null) {
            return;
        }
        if (bidCode == ADSuyiBidLossCode.BID_WIN) {
            BidPriceUtil.sendWin(rewardVideoAD, hbPriceList);
        } else {
            BidPriceUtil.sendLoss(rewardVideoAD, bidCode, hbPriceList);
        }
    }

    @Override
    public void adapterShow(Context context) {
        if (rewardVideoAD != null) {
            rewardVideoAD.showAD();
        }
    }

    @Override
    public void adapterRelease() {
        rewardVideoAD = null;
    }
}
