package cn.admobiletop.adsuyi.adapter.gdt.bean;

import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.qq.e.ads.cfg.VideoOption;
import com.qq.e.ads.nativ.NativeADEventListener;
import com.qq.e.ads.nativ.NativeADMediaListener;
import com.qq.e.ads.nativ.NativeUnifiedADAppMiitInfo;
import com.qq.e.ads.nativ.NativeUnifiedADData;
import com.qq.e.ads.nativ.widget.NativeAdContainer;
import com.qq.e.comm.constants.AdPatternType;
import com.qq.e.comm.util.AdError;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cn.admobiletop.adsuyi.ad.adapter.bean.ADNativeInfo;
import cn.admobiletop.adsuyi.ad.entity.ADSuyiActionType;
import cn.admobiletop.adsuyi.ad.entity.ADSuyiAppInfo;
import cn.admobiletop.adsuyi.adapter.gdt.R;
import cn.admobiletop.adsuyi.adapter.gdt.utils.BidPriceUtil;
import cn.admobiletop.adsuyi.adapter.gdt.utils.VideoOptionUtil;
import cn.admobiletop.adsuyi.adapter.gdt.widget.CustomizeMediaView;
import cn.admobiletop.adsuyi.bid.ADSuyiBidLossCode;

/**
 * @author maipian
 * @description 描述
 * @date 10/14/24
 */
public class NativeInfo extends ADNativeInfo<NativeUnifiedADData> implements
        NativeADEventListener,
        NativeADMediaListener
{
    private VideoOption videoOption;
    private CustomizeMediaView customizeMediaView;
    private boolean isMute;

    public NativeInfo(NativeUnifiedADData adInfo, boolean isMute) {
        super(adInfo);
        this.isMute = isMute;
        if (adInfo != null) {
            setTitle(adInfo.getTitle());
            setDesc(adInfo.getDesc());
            setActionType(adInfo.getAdPatternType());
            setCtaText(ADSuyiActionType.getActionTex(getActionType(), getCtaText() == null ? null : adInfo.getCTAText()));
            if (adInfo.getAppMiitInfo() != null) {
                ADSuyiAppInfo appInfo = new ADSuyiAppInfo();
                NativeUnifiedADAppMiitInfo gdtAppinfo = getAdInfo().getAppMiitInfo();
                appInfo.setName(gdtAppinfo.getAppName());
                appInfo.setDeveloper(gdtAppinfo.getAuthorName());
                appInfo.setVersion(gdtAppinfo.getVersionName());
                appInfo.setPermissionsUrl(gdtAppinfo.getPermissionsUrl());
                appInfo.setPrivacyUrl(gdtAppinfo.getPrivacyAgreement());
                appInfo.setDescriptionUrl(gdtAppinfo.getDescriptionUrl());
                appInfo.setIcp(gdtAppinfo.getIcpNumber());
                appInfo.setSize(gdtAppinfo.getPackageSizeBytes());
                setAppInfo(appInfo);
            }
            setIconUrl(adInfo.getIconUrl());
            setImageUrl(getGdtImageUrl());
            setImageUrlList(adInfo.getImgList());
            setIsVideo(AdPatternType.NATIVE_VIDEO == adInfo.getAdPatternType());
            setHasMediaView(AdPatternType.NATIVE_VIDEO == adInfo.getAdPatternType());
            setPlatformIcon(R.drawable.adsuyi_gdt_platform_icon);

            if (isVideo()) {
                videoOption = VideoOptionUtil.getVideoOption(isMute);
            }
        }
    }

    @Override
    public void onADExposed() {
        callExpose();
    }

    @Override
    public void onADClicked() {
        callClick();
    }

    @Override
    public void onADError(AdError adError) {

    }

    @Override
    public void onADStatusChanged() {

    }

    @Override
    public void onVideoInit() {

    }

    @Override
    public void onVideoLoading() {
        callVideoLoad();
    }

    @Override
    public void onVideoReady() {

    }

    @Override
    public void onVideoLoaded(int i) {

    }

    @Override
    public void onVideoStart() {
        callVideoStart();
    }

    @Override
    public void onVideoPause() {
        callVideoPause();
    }

    @Override
    public void onVideoResume() {

    }

    @Override
    public void onVideoCompleted() {
        callVideoFinish();
    }

    @Override
    public void onVideoError(AdError adError) {
        if (adError != null) {
            callVideoError(adError.getErrorCode(), adError.getErrorMsg());
        }
    }

    @Override
    public void onVideoStop() {

    }

    @Override
    public void onVideoClicked() {

    }

    /**
     * 获取广点通图片地址
     *
     * @return : 图片地址
     */
    private String getGdtImageUrl() {
        if (getAdInfo() != null) {
            int adPatternType = getAdInfo().getAdPatternType();
            if (AdPatternType.NATIVE_3IMAGE == adPatternType) {
                List<String> imgList = getAdInfo().getImgList();
                if (imgList != null && imgList.size() > 0) {
                    return imgList.get(0);
                }
            } else {
                return getAdInfo().getImgUrl();
            }
        }
        return null;
    }

    @Override
    public View getMediaView(ViewGroup container) {
        if (!isVideo()) {
            return null;
        }
        if (customizeMediaView == null) {
            customizeMediaView = new CustomizeMediaView(container.getContext());
        }
        return customizeMediaView;
    }

    @Override
    public void registerViewForInteraction(ViewGroup viewGroup, View... actionViews) {
        if (viewGroup == null || getAdInfo() == null) {
            return;
        }
        if (viewGroup instanceof NativeAdContainer) {
            getAdInfo().setNativeAdEventListener(this);
            List<View> viewList = null;
            if (actionViews != null && actionViews.length > 0) {
                viewList = Arrays.asList(actionViews);
            }
            getAdInfo().bindAdToView(viewGroup.getContext(), (NativeAdContainer) viewGroup, new FrameLayout.LayoutParams(0, 0), viewList);
            if (customizeMediaView != null && customizeMediaView.getMediaView() != null && isVideo()) {
                getAdInfo().bindMediaView(customizeMediaView.getMediaView(), videoOption, this);
            }
            // 设置广告播放时静音
            getAdInfo().setVideoMute(isMute);
        }
    }

    @Override
    public void registerCloseView(View closeView) {
        if (closeView == null) {
            return;
        }
        closeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callClose();
            }
        });
    }

    @Override
    public void adapterBiddingResult(int bidCode, ArrayList<Double> hbPriceList) {
        if (bidCode == ADSuyiBidLossCode.BID_WIN) {
            BidPriceUtil.sendWin(getAdInfo(), hbPriceList);
        } else {
            BidPriceUtil.sendLoss(getAdInfo(), bidCode, hbPriceList);
        }
    }
}
