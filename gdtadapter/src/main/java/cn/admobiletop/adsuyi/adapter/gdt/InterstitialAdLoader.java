package cn.admobiletop.adsuyi.adapter.gdt;

import android.app.Activity;
import android.content.Context;

import com.qq.e.ads.cfg.VideoOption;
import com.qq.e.ads.interstitial2.UnifiedInterstitialAD;
import com.qq.e.ads.interstitial2.UnifiedInterstitialADListener;
import com.qq.e.comm.util.AdError;

import java.util.ArrayList;

import cn.admobiletop.adsuyi.ad.adapter.bean.ADExtraData;
import cn.admobiletop.adsuyi.ad.adapter.loader.ADInterstitialLoader;
import cn.admobiletop.adsuyi.adapter.gdt.utils.BidPriceUtil;
import cn.admobiletop.adsuyi.bid.ADSuyiBidLossCode;

/**
 * @author maipian
 * @description 描述
 * @date 10/12/24
 */
public class InterstitialAdLoader extends ADInterstitialLoader {

    private UnifiedInterstitialAD unifiedInterstitialAD;

    @Override
    public void adapterLoadAd(Context context, String positionId, ADExtraData adExtraData) {
        // 设置视频类插屏广告的参数
        VideoOption option = new VideoOption.Builder()
                // autoPlayMuted，指定视频自动播放时是否静音，可选项包括：true（自动播放时静音），false（自动播放时有声），默认值为 true。
                .setAutoPlayMuted(adExtraData.isMute())
                //autoPlayPolicy，指定不同网络下的视频播放策略，可选项包括：AutoPlayPolicy.WIFI（WiFi 网络自动播放，4G 网络手动点击播放），AutoPlayPolicy.ALWAYS（WiFi 和4G 网络都自动播放），默认值为 AutoPlayPolicy.ALWAYS。
                .setAutoPlayPolicy(VideoOption.AutoPlayPolicy.WIFI)
                .build();

        unifiedInterstitialAD = new UnifiedInterstitialAD(
                (Activity) context,
                positionId,
                new UnifiedInterstitialADListener() {
                    @Override
                    public void onADReceive() {
                        callSuccess(unifiedInterstitialAD.getECPM());
                    }

                    @Override
                    public void onVideoCached() {

                    }

                    @Override
                    public void onNoAD(AdError adError) {
                        if (adError != null) {
                            callFailed(adError.getErrorCode(), adError.getErrorMsg());
                        }
                    }

                    @Override
                    public void onADOpened() {

                    }

                    @Override
                    public void onADExposure() {
                        callExpose();
                    }

                    @Override
                    public void onADClicked() {
                        callClick();
                    }

                    @Override
                    public void onADLeftApplication() {

                    }

                    @Override
                    public void onADClosed() {
                        callClose();
                    }

                    @Override
                    public void onRenderSuccess() {

                    }

                    @Override
                    public void onRenderFail() {

                    }
                }
        );
        unifiedInterstitialAD.setVideoOption(option);
        unifiedInterstitialAD.loadAD();
    }

    @Override
    public void adapterBiddingResult(int bidCode, ArrayList<Double> hbPriceList) {
        if (unifiedInterstitialAD == null) {
            return;
        }
        if (bidCode == ADSuyiBidLossCode.BID_WIN) {
            BidPriceUtil.sendWin(unifiedInterstitialAD, hbPriceList);
        } else {
            BidPriceUtil.sendLoss(unifiedInterstitialAD, bidCode, hbPriceList);
        }
    }

    @Override
    public void adapterShow(Context context) {
        if (unifiedInterstitialAD != null) {
            unifiedInterstitialAD.show();
        }
    }

    @Override
    public void adapterRelease() {
        unifiedInterstitialAD = null;
    }
}
