package cn.admobiletop.adsuyi.adapter.gdt;

import android.content.Context;

import com.qq.e.comm.managers.GDTAdSdk;
import com.qq.e.comm.managers.setting.GlobalSetting;

import java.util.HashMap;
import java.util.Map;

import cn.admobiletop.adsuyi.ad.adapter.ADBaseIniter;
import cn.admobiletop.adsuyi.config.ADSuyiInitConfig;
import cn.admobiletop.adsuyi.util.ADSuyiLogUtil;

/**
 * @author maipian
 * @description 优量汇平台初始化类
 * @date 10/12/24
 */
public class ADSuyiIniter extends ADBaseIniter {

    @Override
    public void init(Context context, String appId, String appKey, ADSuyiInitConfig config) {
        // 设置广点通是否支持隐私政策
        GlobalSetting.setAgreePrivacyStrategy(config == null || config.isAgreePrivacyStrategy());
        // 是否可使用设备id
        GlobalSetting.setAgreeReadDeviceId(config == null || config.isCanUsePhoneState());
        // 是否可使用安卓id
        GlobalSetting.setAgreeReadAndroidId(config == null || config.isCanUsePhoneState());
        // 是否允许读取安装列表
        GlobalSetting.setEnableCollectAppInstallStatus(config == null || config.isCanReadInstallList());
        // 关闭个性化推荐广告，1-屏蔽个性化推荐广告 0、所有非1对值-不屏蔽个性化推荐广告
//            GlobalSetting.setPersonalizedState((config == null || config.isAgreePrivacyStrategy()) ? 0 : 1);

        Map<String, Boolean> params = new HashMap();
        // 优量汇SDK将不采集mac地址
        params.put("mac_address", config == null || config.isCanUseWifiState());
        params.put("bssid", config == null || config.isCanUseWifiState());
        params.put("ssid", config == null || config.isCanUseWifiState());
        // 允许优量汇SDK采集android_id
        params.put("android_id", config == null || config.isCanUsePhoneState());
        // 不设置device_id默认为允许
        params.put("device_id", config == null || config.isCanUsePhoneState());
        // 目前只会获取粗略地理位置信息，禁止读取经纬度坐标
        params.put("cell_id", config == null || config.isCanUseLocation());
        GlobalSetting.setAgreeReadPrivacyInfo(params);

        Map convOptimizeInfoParams = new HashMap();
        convOptimizeInfoParams.put("hieib", false);
        GlobalSetting.setConvOptimizeInfo(convOptimizeInfoParams);

        // 广点通广告SDK初始化方法
        GDTAdSdk.initWithoutStart(context, appId);
        GDTAdSdk.start(new GDTAdSdk.OnStartListener() {
            @Override
            public void onStartSuccess() {
                ADSuyiLogUtil.d("gdtadapter init success");
                callInitSuccess();
            }

            @Override
            public void onStartFailed(Exception e) {
                ADSuyiLogUtil.d("gdtadapter init fail msg: " + e.toString());
                callInitFailed();
            }
        });
        callInitSuccess();
    }

    @Override
    public String getAdapterVersion() {
        return "4.603.1473.11281";
    }

    @Override
    public void setPersonalizedAdEnabled(boolean personalizedAdEnabled) {
        GlobalSetting.setPersonalizedState(personalizedAdEnabled ? 0 : 1);
    }

    /**
     * 是否支持客户端竞价
     * @return
     */
    @Override
    public boolean isClientBid() {
        return true;
    }
}
