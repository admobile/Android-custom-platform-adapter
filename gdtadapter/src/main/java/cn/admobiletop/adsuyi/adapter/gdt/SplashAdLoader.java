package cn.admobiletop.adsuyi.adapter.gdt;

import android.content.Context;
import android.view.ViewGroup;

import com.qq.e.ads.splash.SplashAD;
import com.qq.e.ads.splash.SplashADListener;
import com.qq.e.comm.listeners.ADRewardListener;
import com.qq.e.comm.util.AdError;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.admobiletop.adsuyi.ad.adapter.bean.ADExtraData;
import cn.admobiletop.adsuyi.ad.adapter.loader.ADSplashLoader;
import cn.admobiletop.adsuyi.adapter.gdt.utils.BidPriceUtil;
import cn.admobiletop.adsuyi.bid.ADSuyiBidLossCode;

/**
 * @author maipian
 * @description 描述
 * @date 10/12/24
 */
public class SplashAdLoader extends ADSplashLoader {
    private SplashAD splashAD;
    private long millisUntilFinished;
    private List<Long> aDTickList; // 倒计时不重复回调集合


    @Override
    public void adapterLoadAd(Context context, String positionId, ADExtraData adExtraData) {
        splashAD = new SplashAD(
                context,
                positionId,
                new SplashADListener() {
                    @Override
                    public void onADDismissed() {
                        if (millisUntilFinished / 1000 > 0) {
                            callSkip();
                        }
                        callClose();
                    }

                    @Override
                    public void onNoAD(AdError adError) {
                        if (adError != null) {
                            callFailed(adError.getErrorCode(), adError.getErrorMsg());
                        }
                    }

                    @Override
                    public void onADPresent() {

                    }

                    @Override
                    public void onADClicked() {
                        callClick();
                    }

                    @Override
                    public void onADTick(long l) {
                        millisUntilFinished = l;
                        long seconds = Math.round(millisUntilFinished / 1000f);
                        if (aDTickList == null) {
                            aDTickList = new ArrayList<>();
                        }
                        if (!aDTickList.contains(seconds)) {
                            callTick(seconds);
                            aDTickList.add(seconds);
                        }
                    }

                    @Override
                    public void onADExposure() {
                        callExpose();
                    }

                    @Override
                    public void onADLoaded(long l) {
                        if (splashAD != null) {
                            callSuccess(splashAD.getECPM());
                        }
                    }
                },
                5000
        );
        splashAD.setRewardListener(new ADRewardListener() {
            @Override
            public void onReward(Map<String, Object> map) {
                callReward();
            }
        });
        splashAD.fetchAdOnly();
    }

    @Override
    public void adapterShow(ViewGroup container) {
        if (splashAD != null && container != null) {
            splashAD.showAd(container);
        }
    }

    @Override
    public void adapterBiddingResult(int bidCode, ArrayList<Double> hbPriceList) {
        // 正常开屏
        if (splashAD == null) {
            return;
        }
        if (bidCode == ADSuyiBidLossCode.BID_WIN) {
            BidPriceUtil.sendWin(splashAD, hbPriceList);
        } else {
            BidPriceUtil.sendLoss(splashAD, bidCode, hbPriceList);
        }
    }

    @Override
    public void adapterRelease() {
        splashAD = null;
    }
}
