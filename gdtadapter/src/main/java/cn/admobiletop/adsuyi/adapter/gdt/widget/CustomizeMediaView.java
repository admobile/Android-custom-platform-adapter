package cn.admobiletop.adsuyi.adapter.gdt.widget;

import android.content.Context;
import android.widget.FrameLayout;

import com.qq.e.ads.nativ.MediaView;

import cn.admobiletop.adsuyi.ad.data.IBaseRelease;
import cn.admobiletop.adsuyi.util.ADSuyiViewUtil;

/**
 * @author ciba
 * @description 自定义视频控件
 * @date 2020/5/12
 */
public class CustomizeMediaView extends FrameLayout implements IBaseRelease {
    private MediaView mediaView;
    private boolean idle = true;
    private long idleTime;

    public CustomizeMediaView(Context context) {
        super(context);
        mediaView = new MediaView(context);
        addView(mediaView);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        idle = false;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        idle = true;
        idleTime = System.currentTimeMillis();
    }

    @Override
    public void release() {
        ADSuyiViewUtil.removeSelfFromParent(mediaView);
        mediaView = null;
    }

    /**
     * 获取视频控件
     *
     * @return 视频控件
     */
    public MediaView getMediaView() {
        return mediaView;
    }

    /**
     * 判断是否是空闲状态
     *
     * @return 是否是空闲状态
     */
    public boolean isIdle() {
        return idle;
    }

    /**
     * 获取空闲下来的时间
     *
     * @return 空闲下来的时间
     */
    public long getIdleTime() {
        return idleTime;
    }
}
