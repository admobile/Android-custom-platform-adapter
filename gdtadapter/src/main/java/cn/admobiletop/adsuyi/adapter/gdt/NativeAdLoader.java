package cn.admobiletop.adsuyi.adapter.gdt;

import android.content.Context;

import com.qq.e.ads.cfg.VideoOption;
import com.qq.e.ads.nativ.ADSize;
import com.qq.e.ads.nativ.NativeADUnifiedListener;
import com.qq.e.ads.nativ.NativeExpressAD;
import com.qq.e.ads.nativ.NativeExpressADView;
import com.qq.e.ads.nativ.NativeUnifiedAD;
import com.qq.e.ads.nativ.NativeUnifiedADData;
import com.qq.e.comm.util.AdError;

import java.util.ArrayList;
import java.util.List;

import cn.admobiletop.adsuyi.ad.adapter.bean.ADExtraData;
import cn.admobiletop.adsuyi.ad.adapter.loader.ADNativeLoader;
import cn.admobiletop.adsuyi.adapter.gdt.bean.NativeExpressInfo;
import cn.admobiletop.adsuyi.adapter.gdt.bean.NativeInfo;

/**
 * @author maipian
 * @description 描述
 * @date 10/14/24
 */
public class NativeAdLoader extends ADNativeLoader {

    private NativeExpressAD nativeExpressAD;

    private List<NativeExpressInfo> mNativeExpressInfos;

    private List<NativeInfo> mNativeInfos;

    @Override
    public void adapterLoadExpressAd(Context context, String positionId, ADExtraData adExtraData) {
        int width = ADSize.FULL_WIDTH;
        int height = ADSize.AUTO_HEIGHT;

        if (adExtraData.getAdWidth() > 0) {
            width = adExtraData.getAdWidth();
        }

        if (adExtraData.getAdHeight() > 0) {
            height = adExtraData.getAdHeight();
        }
        boolean isMute = adExtraData.isMute();

        nativeExpressAD = new NativeExpressAD(
                context,
                new ADSize(width, height),
                positionId,
                new NativeExpressAD.NativeExpressADListener() {
                    @Override
                    public void onADLoaded(List<NativeExpressADView> list) {
                        mNativeExpressInfos = new ArrayList<>();
                        if (list != null && list.size() > 0) {
                            for (int i = 0; i < list.size(); i++) {
                                NativeExpressInfo nativeExpressInfo = new NativeExpressInfo(list.get(i));
                                nativeExpressInfo.setEcpm(list.get(i).getECPM());
                                mNativeExpressInfos.add(nativeExpressInfo);
                            }
                        }
                        callSuccess(mNativeExpressInfos);
                    }

                    @Override
                    public void onRenderFail(NativeExpressADView nativeExpressADView) {
                        NativeExpressInfo nativeExpressInfo = getNativeExpressInfo(mNativeExpressInfos, nativeExpressADView);
                        if (nativeExpressInfo != null) {
                            nativeExpressInfo.callRenderFailed(-1, "unknown");
                        }
                    }

                    @Override
                    public void onRenderSuccess(NativeExpressADView nativeExpressADView) {

                    }

                    @Override
                    public void onADExposure(NativeExpressADView nativeExpressADView) {
                        NativeExpressInfo nativeExpressInfo = getNativeExpressInfo(mNativeExpressInfos, nativeExpressADView);
                        if (nativeExpressInfo != null) {
                            nativeExpressInfo.callExpose();
                        }
                    }

                    @Override
                    public void onADClicked(NativeExpressADView nativeExpressADView) {
                        NativeExpressInfo nativeExpressInfo = getNativeExpressInfo(mNativeExpressInfos, nativeExpressADView);
                        if (nativeExpressInfo != null) {
                            nativeExpressInfo.callClick();
                        }
                    }

                    @Override
                    public void onADClosed(NativeExpressADView nativeExpressADView) {
                        NativeExpressInfo nativeExpressInfo = getNativeExpressInfo(mNativeExpressInfos, nativeExpressADView);
                        if (nativeExpressInfo != null) {
                            nativeExpressInfo.callClose();
                        }
                    }

                    @Override
                    public void onADLeftApplication(NativeExpressADView nativeExpressADView) {

                    }

                    @Override
                    public void onNoAD(AdError adError) {
                        if (adError != null) {
                            callFailed(adError.getErrorCode(), adError.getErrorMsg());
                        }
                    }
                }
        );

        // 注意：如果您在联盟平台上新建原生模板广告位时，选择了支持视频，那么可以进行个性化设置（可选）
        nativeExpressAD.setVideoOption(new VideoOption.Builder()
                // WIFI 环境下可以自动播放视频
                .setAutoPlayPolicy(VideoOption.AutoPlayPolicy.WIFI)
                // 自动播放时为静音
                .setAutoPlayMuted(isMute)
                .build());

        nativeExpressAD.loadAD(adExtraData.getAdCount());
    }

    @Override
    public void adapterLoadNativeAd(Context context, String positionId, final ADExtraData adExtraData) {
        NativeUnifiedAD nativeUnifiedAd = new NativeUnifiedAD(
                context,
                positionId,
                new NativeADUnifiedListener() {
                    @Override
                    public void onADLoaded(List<NativeUnifiedADData> list) {
                        mNativeInfos = new ArrayList<>();
                        if (list != null && list.size() > 0) {
                            for (int i = 0; i < list.size(); i++) {
                                NativeInfo nativeInfo = new NativeInfo(list.get(i), adExtraData.isMute());
                                nativeInfo.setEcpm(list.get(i).getECPM());
                                mNativeInfos.add(nativeInfo);
                            }
                        }
                        callSuccess(mNativeInfos);
                    }

                    @Override
                    public void onNoAD(AdError adError) {
                        if (adError != null) {
                            callFailed(adError.getErrorCode(), adError.getErrorMsg());
                        }
                    }
                }
        );

        // 加载广告
        nativeUnifiedAd.loadData(adExtraData.getAdCount());
    }

    private NativeExpressInfo getNativeExpressInfo(List<NativeExpressInfo> nativeInfos, NativeExpressADView nativeExpressAdInfo) {
        if (nativeInfos != null && nativeInfos.size() > 0) {
            for (int i = 0; i < nativeInfos.size(); i++) {
                NativeExpressInfo nativeExpressInfo = nativeInfos.get(i);
                if (nativeExpressInfo != null
                        && nativeExpressInfo.getAdInfo() != null
                        && nativeExpressInfo.getAdInfo() == nativeExpressAdInfo) {
                    return nativeExpressInfo;
                }
            }
        }
        return null;
    }

    @Override
    public void adapterRelease() {
    }
}
