package cn.admobiletop.adsuyi.adapter.gdt.bean;

import android.view.View;
import android.view.ViewGroup;

import com.qq.e.ads.nativ.NativeExpressADView;
import com.qq.e.ads.nativ.NativeExpressMediaListener;
import com.qq.e.comm.util.AdError;

import java.util.ArrayList;

import cn.admobiletop.adsuyi.ad.adapter.bean.ADNativeExpressInfo;
import cn.admobiletop.adsuyi.adapter.gdt.utils.BidPriceUtil;
import cn.admobiletop.adsuyi.bid.ADSuyiBidLossCode;


/**
 * @author maipian
 * @description 描述
 * @date 10/14/24
 */
public class NativeExpressInfo extends ADNativeExpressInfo<NativeExpressADView> implements NativeExpressMediaListener {

    public NativeExpressInfo(NativeExpressADView adInfo) {
        super(adInfo);
        getAdInfo().setMediaListener(this);
    }

    @Override
    public View getNativeExpressAdView(ViewGroup container) {
        return getAdInfo();
    }

    @Override
    public void render(ViewGroup container) {
        if (getAdInfo() != null) {
            getAdInfo().render();
        }
    }

    @Override
    public void onVideoInit(NativeExpressADView nativeExpressADView) {

    }

    @Override
    public void onVideoLoading(NativeExpressADView nativeExpressADView) {
        callVideoLoad();
    }

    @Override
    public void onVideoCached(NativeExpressADView nativeExpressADView) {
    }

    @Override
    public void onVideoReady(NativeExpressADView nativeExpressADView, long l) {

    }

    @Override
    public void onVideoStart(NativeExpressADView nativeExpressADView) {
        callVideoStart();
    }

    @Override
    public void onVideoPause(NativeExpressADView nativeExpressADView) {
        callVideoPause();
    }

    @Override
    public void onVideoComplete(NativeExpressADView nativeExpressADView) {
        callVideoFinish();
    }

    @Override
    public void onVideoError(NativeExpressADView nativeExpressADView, AdError adError) {
        callVideoError(adError.getErrorCode(), adError.getErrorMsg());
    }

    @Override
    public void onVideoPageOpen(NativeExpressADView nativeExpressADView) {

    }

    @Override
    public void onVideoPageClose(NativeExpressADView nativeExpressADView) {

    }

    @Override
    public void adapterBiddingResult(int bidCode, ArrayList<Double> hbPriceList) {
        if (bidCode == ADSuyiBidLossCode.BID_WIN) {
            BidPriceUtil.sendWin(getAdInfo(), hbPriceList);
        } else {
            BidPriceUtil.sendLoss(getAdInfo(), bidCode, hbPriceList);
        }
    }
}
