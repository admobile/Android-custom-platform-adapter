package cn.admobiletop.adsuyi.adapter.gdt;

import android.app.Activity;
import android.content.Context;
import android.view.ViewGroup;

import com.qq.e.ads.banner2.UnifiedBannerADListener;
import com.qq.e.ads.banner2.UnifiedBannerView;
import com.qq.e.comm.util.AdError;

import cn.admobiletop.adsuyi.ad.adapter.bean.ADExtraData;
import cn.admobiletop.adsuyi.ad.adapter.loader.ADBannerLoader;

/**
 * @author maipian
 * @description 描述
 * @date 10/12/24
 */
public class BannerAdLoader extends ADBannerLoader {

    private UnifiedBannerView unifiedBannerView;

    @Override
    public void adapterLoadAd(Context context, String positionId, ADExtraData adExtraData) {
        unifiedBannerView = new UnifiedBannerView(
                (Activity) context,
                positionId,
                new UnifiedBannerADListener() {
                    @Override
                    public void onNoAD(AdError adError) {
                        if (adError != null) {
                            callFailed(adError.getErrorCode(), adError.getErrorMsg());
                        }
                    }

                    @Override
                    public void onADReceive() {
                        callSuccess(unifiedBannerView.getECPM());
                    }

                    @Override
                    public void onADExposure() {
                        callExpose();
                    }

                    @Override
                    public void onADClosed() {
                        callClose();
                    }

                    @Override
                    public void onADClicked() {
                        callClick();
                    }

                    @Override
                    public void onADLeftApplication() {

                    }
                }
        );

        unifiedBannerView.loadAD();
    }

    @Override
    public void adapterShow(ViewGroup container) {
        if (unifiedBannerView != null) {
            container.addView(unifiedBannerView);
        }
    }

    @Override
    public void adapterRelease() {
        unifiedBannerView = null;
    }
}
