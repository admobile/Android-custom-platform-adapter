package cn.admobiletop.adsuyi.adapter.gdt.utils;

import com.qq.e.comm.constants.BiddingLossReason;
import com.qq.e.comm.pi.IBidding;
import com.qq.e.comm.pi.LADI;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cn.admobiletop.adsuyi.bid.ADSuyiBidLossCode;

/**
 * @author maipian
 * @description 描述
 * @date 10/14/24
 */
public class BidPriceUtil {
    public static void sendWin(LADI ladi, ArrayList<Double> hbPriceList) {
        // 媒体回传⼆价ecpm，竞价成功后，必须在展示前回传
        if (hbPriceList != null) {
            if (hbPriceList.size() > 1) {
                if (ladi != null) {
                    Double secondPrice = hbPriceList.get(1);
                    //单位：元 转换为 单位：分
                    BigDecimal bigDecimal = BigDecimal.valueOf(secondPrice);
                    BigDecimal secBidPrice = bigDecimal.multiply(new BigDecimal(100));
                    if (secBidPrice != null) {
                        Map<String, Object> winMap = new HashMap<>();
                        winMap.put(IBidding.EXPECT_COST_PRICE, ladi.getECPM());
                        winMap.put(IBidding.HIGHEST_LOSS_PRICE, secBidPrice.intValue());
                        ladi.sendWinNotification(winMap);
                    } else {
                        //兜底策略，一般不会执行，防止异常情况导致后续逻辑无法顺利执行
                        Map<String, Object> winMap = new HashMap<>();
                        winMap.put(IBidding.EXPECT_COST_PRICE, ladi.getECPM());
                        winMap.put(IBidding.HIGHEST_LOSS_PRICE, 0);
                        ladi.sendWinNotification(winMap);
                    }
                }
            } else if (hbPriceList.size() == 1) {
                //如果竞价队列只有优量汇
                if (ladi != null) {
                    //单位：分
                    Map<String, Object> winMap = new HashMap<>();
                    winMap.put(IBidding.EXPECT_COST_PRICE, ladi.getECPM());
                    winMap.put(IBidding.HIGHEST_LOSS_PRICE, 0);
                    ladi.sendWinNotification(winMap);
                }
            }
        } else {
            //单位：分
            if (ladi != null) {
                Map<String, Object> winMap = new HashMap<>();
                winMap.put(IBidding.EXPECT_COST_PRICE, ladi.getECPM());
                winMap.put(IBidding.HIGHEST_LOSS_PRICE, 0);
                ladi.sendWinNotification(winMap);
            }
        }
    }

    public static void sendLoss(LADI ladi, int bidCode, ArrayList<Double> hbPriceList) {
        int highestPrice = 0;
        if (hbPriceList != null && hbPriceList.size() > 0) {
            Double price = hbPriceList.get(0);

            //单位：元 转换为 单位：分
            BigDecimal bigDecimal = BigDecimal.valueOf(price);
            BigDecimal multiplyPrice = bigDecimal.multiply(new BigDecimal(100));
            highestPrice = multiplyPrice.intValue();
        }

        if (ladi != null) {
            /* @param adnID - 本次竞胜方渠道ID，选填；
             * 对于【adnID】字段回传支持3个枚举值，分别代表：
             * 1 - 输给优量汇其它广告；
             * 2 - 输给第三方ADN（无需回传具体竞胜方渠道）；
             * 3 - 输给自售广告主；
             */
            if (bidCode == ADSuyiBidLossCode.BID_PRICE_NOT_HIGHEST) {
                Map<String, Object> lossMap = new HashMap<>(3);
                lossMap.put(IBidding.WIN_PRICE, highestPrice);
                lossMap.put(IBidding.LOSS_REASON, BiddingLossReason.LOW_PRICE);
                lossMap.put(IBidding.ADN_ID, 2);
                ladi.sendLossNotification(lossMap);

            } else if (bidCode == ADSuyiBidLossCode.BID_TIMEOUT) {
                Map<String, Object> lossMap = new HashMap<>(3);
                lossMap.put(IBidding.WIN_PRICE, highestPrice);
                lossMap.put(IBidding.LOSS_REASON, BiddingLossReason.NOT_COMPETITION);
                lossMap.put(IBidding.ADN_ID, 2);
                ladi.sendLossNotification(lossMap);

            } else if (bidCode == ADSuyiBidLossCode.BID_WIN_BUT_NOT_SHOW) {
                Map<String, Object> lossMap = new HashMap<>(3);
                lossMap.put(IBidding.WIN_PRICE, highestPrice);
                lossMap.put(IBidding.LOSS_REASON, BiddingLossReason.OTHER);
                lossMap.put(IBidding.ADN_ID, 2);
                ladi.sendLossNotification(lossMap);

            }
        }
    }
}
