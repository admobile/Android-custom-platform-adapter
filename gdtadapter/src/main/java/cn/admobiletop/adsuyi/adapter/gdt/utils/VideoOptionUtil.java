package cn.admobiletop.adsuyi.adapter.gdt.utils;

import com.qq.e.ads.cfg.VideoOption;

/**
 * @author ciba
 * @description 视频参数工具类
 * @date 2020/4/7
 */
public class VideoOptionUtil {

    public static VideoOption getVideoOption(boolean mute) {
        VideoOption.Builder builder = new VideoOption.Builder();

        // 设置视频广告在预览页自动播放的网络条件：VideoOption.AutoPlayPolicy.WIFI表示只在WiFi下自动播放；
        // VideoOption.AutoPlayPolicy.ALWAYS表示始终自动播放，不区分当前网络；
        // VideoOption.AutoPlayPolicy.ALWAYS表示始终都不自动播放，不区分当前网络，
        // 但在WiFi时会预下载视频资源；默认为始终自动播放；模板渲染视频和自渲染2.0视频都可使用
        builder.setAutoPlayPolicy(VideoOption.AutoPlayPolicy.WIFI);
        // 设置视频广告在预览页自动播放时是否静音，默认为true，静音自动播放；模板渲染视频和自渲染2.0视频都可使用
        builder.setAutoPlayMuted(mute);
        // 设置落地页静音
        builder.setDetailPageMuted(mute);
        // 设置视频广告在预览页未开始播放时是否显示封面图，默认为true，显示封面图；只对自渲染2.0视频广告生效
        builder.setNeedCoverImage(true);
        // 设置视频广告在在预览页播放过程中是否显示进度条，默认为true，显示进度条；只对自渲染2.0视频广告生效
        builder.setNeedProgressBar(true);
        // 用户在预览页点击clickableViews或视频区域(setEnableUserControl设置为false)时是否跳转到详情页，默认为true，跳转到详情页；只对自渲染2.0视频广告生效
        builder.setEnableDetailPage(true);

        return builder.build();
    }

}
